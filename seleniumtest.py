from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

def test_run():
  url= 'http://prajaktabavikar1:Yxhw9y3qDypLkor52nuz@hub.browserstack.com:80/wd/hub' 
  desired_cap = {'os': 'Windows', 'os_version': 'xp', 'browser': 'IE', 'browser_version': '7.0' }
  desired_cap['browserstack.local'] = True
  desired_cap['browserstack.debug'] = True 
  driver = webdriver.Remote(command_executor=url, desired_capabilities=desired_cap)
  
  driver.get('http://127.0.0.1:8087/Courses')
  print driver.title
  if not "PLTL" in driver.title:
    raise Exception("Are you not on PLTL? How come!")
  loginButton = driver.find_element_by_id("loginButton")
  loginButton.click()

  driver.find_element_by_xpath('.//*[@id="emailInput"]').send_keys("mgross@email.com")
  pwd_field = driver.find_element_by_xpath('.//*[@id="LoginModal"]/form/div[2]/div/label/input')
  pwd_field.send_keys("goat")
  submitLogin = driver.find_element_by_id("submitLogin")
  submitLogin.click()
  body = driver.find_element_by_tag_name('body')
  assert "Search" in body.text, "Login Tested"
  assert "Create" in body.text, "Login Tested"
  create_tab = driver.find_element_by_xpath('.//*[@id="classTab"]')
  create_tab.click()
  class_name = driver.find_element_by_xpath('.//*[@id="class_id"]')
  class_name.send_keys("csc102Spring2015")
  course_select = driver.find_element_by_xpath('.//*[@id="classTab"]/div[1]/section/form/div[1]/div[2]/select/option[2]')
  course_select.click()
  semester = driver.find_element_by_xpath('.//*[@id="classTab"]/div[1]/section/form/div[2]/div[1]/select/option[2]')
  semester.click()
  year = driver.find_element_by_xpath('.//*[@id="year"]')
  year.send_keys("2015")
  class_description = driver.find_element_by_xpath('.//*[@id="class_description"]')
  class_description.send_keys("description of the class goes here")
  createButton = driver.find_element_by_xpath('.//*[@id="btnNew"]')
  createButton.click()
  class_created = driver.find_element_by_xpath('.//*[@id="classTab"]/div[3]/table/tbody/tr[2]/td[1]').text
  assert "csc102Spring2015" in class_created
  driver.quit()
